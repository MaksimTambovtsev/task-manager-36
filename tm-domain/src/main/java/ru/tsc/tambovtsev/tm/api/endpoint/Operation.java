package ru.tsc.tambovtsev.tm.api.endpoint;

import ru.tsc.tambovtsev.tm.dto.request.AbstractRequest;
import ru.tsc.tambovtsev.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
