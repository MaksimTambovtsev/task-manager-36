package ru.tsc.tambovtsev.tm.exception.system;

import ru.tsc.tambovtsev.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument ``" + argument + "`` is not supported...");
    }

}
