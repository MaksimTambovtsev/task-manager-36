package ru.tsc.tambovtsev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.tambovtsev.tm.api.endpoint.*;
import ru.tsc.tambovtsev.tm.api.repository.ICommandRepository;
import ru.tsc.tambovtsev.tm.api.service.*;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;
import ru.tsc.tambovtsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.tambovtsev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.tambovtsev.tm.repository.CommandRepository;
import ru.tsc.tambovtsev.tm.service.*;
import ru.tsc.tambovtsev.tm.util.SystemUtil;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);


    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final String packageCommands = AbstractCommand.class.getPackage().getName();
        @NotNull final Reflections reflections = new Reflections(packageCommands);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz: classes) {
            registry(clazz);
        }
    }

    @SneakyThrows
    private void registry(@NotNull final Class clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final Object object = clazz.newInstance();
        @NotNull final AbstractCommand command = (AbstractCommand) object;
        registry(command);
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
    }


    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

}