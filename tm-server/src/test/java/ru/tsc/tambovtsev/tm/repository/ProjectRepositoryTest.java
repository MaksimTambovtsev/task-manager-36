package ru.tsc.tambovtsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.List;


public final class ProjectRepositoryTest {

    private final IProjectRepository projectRepository = new ProjectRepository();

    @Before
    public void setProjectRepository() {
        projectRepository.create("123", "432", "222");
        projectRepository.create("1321", "234", "555");
    }

    @After
    public void clearProjectRepository() {
        projectRepository.clear();
    }

    @Test
    public void testFindAll() {
        Assert.assertFalse(projectRepository.findAll().isEmpty());
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertTrue(projectRepository.findAll().get(0).getName().isEmpty());
    }

    @Test
    public void testFindById() {
        @Nullable final List<Project> projects = projectRepository.findAll();
        @Nullable final String projectId = projects.stream().findFirst().get().getId();
        Assert.assertFalse(projectRepository.findById(projectId).getName().isEmpty());
    }

    @Test
    public void testAdd() {
        @NotNull final ProjectRepository projectRepository1 = new ProjectRepository();
        projectRepository1.add(projectRepository.findAll().get(1));
        @Nullable final Project projectExp = projectRepository.findAll().get(1);
        @Nullable final Project projectAct = projectRepository1.findAll().get(0);
        Assert.assertEquals(projectExp.getId(), projectAct.getId());
        Assert.assertEquals(projectExp.getName(), projectAct.getName());
        Assert.assertEquals(projectExp.getDescription(), projectAct.getDescription());
    }

    @Test
    public void testRemove() {
        @NotNull final Project project = projectRepository.findAll().get(1);
        projectRepository.remove(project);
        Assert.assertNotEquals(project.getId(), projectRepository.findAll().get(0).getId());
    }

    @Test
    public void testRemoveById() {
        @NotNull final Project project = projectRepository.findAll().get(1);
        projectRepository.removeById(project.getId());
        Assert.assertNotEquals(project.getId(), projectRepository.findAll().get(0).getId());
    }

    @Test
    public void testExistById() {
        @NotNull final Project projectFirst = projectRepository.findAll().get(0);
        Assert.assertTrue(projectRepository.existsById(projectFirst.getId()));
    }

    @Test
    public void testClear() {
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

}
